#!/usr/bin/env bash

echo -n "Your git username: "
read username

echo -n "Your git password: "
read -s password
echo "*********"

echo "-------- CONFIG ----------"

echo "Your title [length >= 6]:"
read customer_title

echo "Your customer [length >= 6]:"
read customer_name

echo "Customer email [length >= 8]:"
read customer_email

echo "Customer password [length >= 8]:"
read -s customer_password
echo "*********"

echo "Free port from 3000 to 4000:"
read customer_port

echo "-------- START ----------"

git clone -b prodaction "https://$username:$password@bitbucket.org/eskimo___/insitu-v.git"
git clone -b prodaction "https://$username:$password@bitbucket.org/eskimo___/insitu-be.git"

node ./deploy.js "$customer_title" "$customer_name" "$customer_email" "$customer_password" "$customer_port"

cd insitu-v
npm install
npm run build
cd ..

cd insitu-be
cd output
tar -zxf insitu-be.tar.gz
cd bundle/programs/server
npm install
cd ../../../../
cd ..

pm2 start deploy.json

echo "-------- END ----------"
ls
