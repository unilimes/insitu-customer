#!/usr/bin/env bash

read -p "Are you sure? [Y/n]: " -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
    folder=${PWD##*/}

    pm2 stop deploy.json
    pm2 delete deploy.json

    cd ..
    rm -rf "$folder"
    cd ..
    ls
fi
