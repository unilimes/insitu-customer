#!/usr/bin/env bash

echo "-------- START ----------"

cd insitu-v
git pull
rm -rf public
npm install
npm run build
cd ..

cd insitu-be
git pull
cd output
rm -rf bundle
tar -zxf insitu-be.tar.gz
cd bundle/programs/server
npm install
cd ../../../../
cd ..

pm2 restart deploy.json

echo "-------- END ----------"
ls
