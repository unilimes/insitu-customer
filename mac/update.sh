#!/usr/bin/env bash

echo "-------- START ----------"

cd insitu-v
git pull
rm -rf public
npm install
cd ..

cd insitu-be
git pull
meteor npm install
cd ..

echo "-------- END ----------"
ls
