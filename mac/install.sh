#!/usr/bin/env bash

echo -n "Your git username: "
read username

echo -n "Your git password: "
read -s password
echo "*********"

echo "-------- START ----------"

git clone -b prodaction "https://$username:$password@bitbucket.org/eskimo___/insitu-v.git"
git clone -b prodaction "https://$username:$password@bitbucket.org/eskimo___/insitu-be.git"

cd insitu-v
npm install
cd ..

cd insitu-be
meteor npm install
cd ..

echo "-------- END ----------"
ls
