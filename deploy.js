var fs = require('fs');
var path = require('path');

var args = process.argv.slice(2);
var folder = path.join(__dirname, '/');

if (args.length < 4) {
    console.log('Failed args:', args);
    process.exit(1);
}

var title = args[0];
var customer = args[1];
var email = args[2];
var password = args[3];
var port = +args[4];

console.log('Your args:', title, customer, email, password, port);

var privateData = {
    "apps": [
        {
            "name": customer + "-backend",
            "script": './insitu-be/output/bundle/main.js',
            "log_date_format": "YYYY-MM-DD",
            "exec_mode": "fork_mode",
            "env": {
                "PORT": port,
                "ROOT_URL": "http://127.0.0.1",
                "MONGO_URL": "mongodb://127.0.0.1:27017/" + customer + "-backend",
                "AWS_ACCESS_KEY_ID": "AKIAJIEVHKCIBHEKPSWA",
                "AWS_SECRET_ACCESS_KEY": "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",
                "S3_BUCKET_NAME": "insitu-aws-s3-bucket-user-content",
                "EMAIL": email,
                "PASSWORD": password,
                "VIEWER": port + 1000,
                "TITLE": title
            }
        },
        {
            "name": customer + "-viewer",
            "script": './insitu-v/prodaction.js',
            "log_date_format": "YYYY-MM-DD",
            "exec_mode": "fork_mode",
            "env": {
                "PORT": port + 1000
            }
        }
    ]
};

var publicData = {
    "apps": [
        {
            "name": customer + "-backend",
            "log_date_format": "YYYY-MM-DD",
            "exec_mode": "fork_mode",
            "env": {
                "PORT": port,
                "VIEWER": port + 1000,
                "TITLE": title
            }
        },
        {
            "name": customer + "-viewer",
            "log_date_format": "YYYY-MM-DD",
            "exec_mode": "fork_mode",
            "env": {
                "PORT": port + 1000
            }
        }
    ]
};

var privateJson = JSON.stringify(privateData, null, 2);
var publicJson = JSON.stringify(publicData, null, 2);

fs.writeFile(path.join(folder, '/insitu-v/deploy.json'), publicJson, (err) => {
    if (err) {
        throw err;
    }

    fs.writeFile(path.join(folder, '/deploy.json'), privateJson, (err) => {
        if (err) {
            throw err;
        }

        console.log('Deploy config saved!');
    });
});
